/*1. Integer.valueOf(char) convert the char to ASCII value for example, Integer.valueOf('1') returns 49
 *2. To get the true value of a Character, use Character.getNumericaValue(c)
 *3. Use charAt to access the character in a string, or use toCharArray() to convert string to char[]
 *4. Need to check the if the carry is greater then zero at the end*/

public class AddBinary67 {
	public String solution(String a, String b){
	
		int a_len = a.length(), b_len = b.length();
		int a_item, b_item, sum=0, carry = 0;
		String output = "";
		
		while (a_len>0 ||b_len>0 ||carry > 0){
			if(a_len<1)
				a_item = 0;
			else
				a_item = Character.getNumericValue(a.charAt(a_len-1));
			
			if (b_len < 1)
				b_item = 0;
			else
				b_item = Character.getNumericValue(b.charAt(b_len-1));
			
			sum = carry + a_item + b_item;
			
			if (sum == 3){
				carry = 1;
				output += "1";
			}
			else if (sum==2){
				carry = 1;
				output += "0";
			}
			else if (sum == 1){
				carry = 0;
				output += "1";
			}
			else{
				carry = 0;
				output += "0";
			}
			a_len--;
			b_len--;
		}
	
	return new StringBuilder(output).reverse().toString();
	}
	
	public static void main(String[] args){
		String a = "110";
		String b = "110";
		
		AddBinary67 ab = new AddBinary67();
		System.out.println(ab.solution(a,b));
		
	}

}
