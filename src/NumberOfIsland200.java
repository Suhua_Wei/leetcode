import java.util.*;
public class NumberOfIsland200 {
	private int size;
	private HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
	
	public int solution(char[][] grid) {
        int islands = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    dfsMark(grid, i, j);
                    if (map.containsKey(size))
                    		map.put(size, map.get(size)+1);
                    		
                    else
                    		map.put(size, 1);
                    		
                    size = 0;
                    islands++;
                }
            }
        }
        return islands;
    }
    
    private void dfsMark(char[][] grid, int i, int j) {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[i].length) return;
        if (grid[i][j] != '1') return;
        
        grid[i][j] = '0';
        size = size + 1;
        dfsMark(grid, i, j + 1);
        dfsMark(grid, i + 1, j);
        dfsMark(grid, i, j - 1);
        dfsMark(grid, i - 1, j);
    }
    public HashMap<Integer, Integer> getSize(){
    		return map;
    }
    
    public static void main(String[] args) {
    	char[][] m = {{'1','0','0','0'},{'1','1','0','0'}, {'0','0','0','0'},{'1','1','1','1'}};
    NumberOfIsland200 n = new NumberOfIsland200();
    n.solution(m);
    HashMap map = n.getSize();
    Iterator it = n.map.entrySet().iterator();
    while (it.hasNext()) {
    	Map.Entry pair = (Map.Entry) it.next();
    	System.out.println(pair.getKey()+":"+ pair.getValue());
    	it.remove();
    }
    
    }
}
