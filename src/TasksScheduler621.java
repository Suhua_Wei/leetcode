import java.util.*;

public class TasksScheduler621 {
	public static int solution(char[] tasks, int n) {
		int min_interval = 0;
		//count the frequency of the characters using Hashmap
		HashMap<Character, Integer> map = new HashMap<Character, Integer>();
		for (char c : tasks) {
			if(map.containsKey(c))
				map.put(c, map.get(c)+1);
			else
				map.put(c, 1);
		}
		//compute unique letters u, maximum count of letters m, number of letters that has maximum count k
		
		int max_cnt = 0, k=0;
		
		for (Map.Entry<Character, Integer> entry : map.entrySet()) {
			if (entry.getValue() >= max_cnt) {
				//find a letter with greater count
				if(entry.getValue() > max_cnt) {
					// reset m, k, and max_cnt
					k = 1;
					max_cnt = entry.getValue();
				}
				//find additional letter with the same maximum count
				else 
					k +=1;	
			}
		}
		System.out.println("print max_cnt, n, k : "+max_cnt+" "+n+" "+k);
		min_interval = Math.max(tasks.length, (max_cnt-1)*(n+1)+k);
		return min_interval;
	}
	
	public static void main(String[] args) {
		System.out.println("Input a string");
		Scanner input = new Scanner(System.in);
		String s = input.next();
		char[] tasks = s.toCharArray();
		System.out.println("Input interval n:");
		int n = input.nextInt();
		System.out.println("the result is " + solution(tasks,n));
	}

}
