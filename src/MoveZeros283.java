import java.util.*;

public class MoveZeros283 {
	public void solution(int[] nums) {
		        int slow = 0, fast = 1;
		        for (; slow < nums.length && fast < nums.length; slow++) {
		        		if(nums[slow]==0) {
		        			while(fast < nums.length-1 && nums[fast]==0) {
		        				fast++;
		        			}
		        			//swap nums[slow] and nums[fast]
		        			int temp = nums[fast];
		        			nums[fast] = nums[slow];
		        			nums[slow] = temp;
		        		}
		        		
		        		fast++;
		        }
	    }
	
	public static void main(String[] args) {
		System.out.println("type an array of numbers, leading with its size");
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int[] nums =  new int[n];
		for(int i = 0; i < n; i++)
			nums[i] = input.nextInt();
		
		MoveZeros283 a = new MoveZeros283();
		a.solution(nums);
		for(int i=0; i<n; i++)
			System.out.print(nums[i]+" ");
	}

}
