/* Java program for Merge Sort */
import java.util.*;
class MergeSort
{
	Integer merge(int arr[], int l, int m, int r)
	{   
	    for(int k=l; k<=r;k++)
	    		System.out.print(arr[k]+" ");
	    System.out.println("as Input Subarray");
		// Find sizes of two subarrays to be merged
		int n1 = m - l + 1;
		int n2 = r - m;
		int cnt = 0;

		/* Create temp arrays */
		int L[] = new int [n1];
		int R[] = new int [n2];

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i)
			L[i] = arr[l + i];
		for (int j=0; j<n2; ++j)
			R[j] = arr[m + 1+ j];


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays
		int i = 0, j = 0;

		// Initial index of merged subarry array
		int k = l;
		while (i < n1 && j < n2)
		{
			if (L[i] <= R[j])
			{
				arr[k] = L[i];
				i++;
			}
			else
			{
				arr[k] = R[j];
				j++;
				cnt += (n1-i);
			}
			k++;
		}

		/* Copy remaining elements of L[] if any */
		while (i < n1)
		{
			arr[k] = L[i];
			i++;
			k++;
		}

		/* Copy remaining elements of R[] if any */
		while (j < n2)
		{
			arr[k] = R[j];
			j++;
			k++;
		}
		System.out.println("current cnt value is "+cnt);
		return cnt;
	}

	// Main function that sorts arr[l..r] using
	// merge()
	Integer sort(int arr[], int l, int r)
	{
		if (l < r)
		{
			// Find the middle point
			int m = (l+r)/2;

			// Sort first and second halves
			return sort(arr, l, m) + sort(arr , m+1, r) + merge(arr, l, m, r);	
		}
		return 0;
	}

	/* A utility function to print array of size n */
	static void printArray(int arr[])
	{
		int n = arr.length;
		for (int i=0; i<n; ++i)
			System.out.print(arr[i] + " ");
		System.out.println();
	}

	// Driver method
	public static void main(String args[])
	{   
		System.out.println("Input the size of the array:");
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
	    System.out.println("Type n numbers separated by space :");
		int arr[] = new int[n];
		for (int i=0; i < n; i++) {
			arr[i] = input.nextInt();
		}

		MergeSort ob = new MergeSort();
		System.out.println("#### inversion count is " + ob.sort(arr, 0, arr.length-1) + "  ####");

		System.out.println("\nSorted array");
		printArray(arr);
	}
}
