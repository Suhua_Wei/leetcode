/*The Hamming distance between two integers is the number of positions at which the corresponding bits are different.

Given two integers x and y, calculate the Hamming distance.
Example:

Input: x = 1, y = 4

Output: 2

Explanation:
1   (0 0 0 1)
4   (0 1 0 0)

 return Integer.bitCount(x ^ y)
 
*/

public class HammingDistance461 {
	
	public int solution(int x, int y){
		
		int diff = x^y, count=0;
		String str = Integer.toBinaryString(diff);
		for (int i=str.length()-1; i>=0; i--)
			count += Character.getNumericValue(str.charAt(i));
		return count;
		
//	String x_str = Integer.toBinaryString(x);
//	String y_str = Integer.toBinaryString(y);
//	System.out.println(x_str + " " +y_str);
//	int x_len = x_str.length(), y_len = y_str.length(), max_len = Math.max(x_len, y_len), count = 0;
//	Character x_item, y_item;
//	
//	for (int i=max_len-1; i>=0; i--){
//		x_item = (x_len<1)? new Character('0') : (Character)x_str.charAt(x_len-1);
//		y_item = (y_len<1)? new Character('0') : (Character)y_str.charAt(y_len-1);
//		count = x_item.equals(y_item) ?  count : count+1;
//		x_len--;
//		y_len--;
//	}
//   return count;
	}
	
	public static void main(String[] args){
		int x = 2, y = 4;
		HammingDistance461 hd = new HammingDistance461();
		System.out.println(hd.solution(x,y));
		
	}

}
