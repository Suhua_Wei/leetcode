/* 
 * 1.  three steps, save the next node, reverse the node and move forward
 * 2.  return prev node, because the last iteration at current node(null) stops connecting to prev */

public class ReverseLinkedList206 {
	
	public ListNode solution(ListNode head){
		ListNode curr = head;
		ListNode prev = null;
		ListNode next = null;
		while(curr!=null){
			//save the next node
			next = curr.next;
			
			//actually reverse the node
			curr.next = prev;
			
			//move forwards
			prev = curr;
			curr = next;
		}
		return prev;
	}
	
	public static void printNode(ListNode head){
		while (head!=null){
			System.out.println(head.val);
			head = head.next;
		}
	}
	public static void main(String[] args){
		ListNode one = new ListNode(1);
		ListNode two = new ListNode(2);
		ListNode three = new ListNode(3);
		ListNode four= new ListNode(4);
		ListNode five = new ListNode(5);
		ListNode six = new ListNode(6);
		one.next = two;
		two.next = three;
		three.next = four;
		four.next = five;
		five.next = six;

		ReverseLinkedList206 rl = new ReverseLinkedList206();
		ListNode head = rl.solution(one);
		printNode(head);
		
	}
	
	

}
